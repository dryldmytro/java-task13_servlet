package com.dryl;

import static com.dryl.Constant.*;
public class User {
    private static int unique=1;
    private int id;
    private String name;
    private String order;

    public User(String name, String order) {
        this.name = name;
        this.order=order;
        id = unique;
        list.add(this);
        unique++;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrder() {
        return order;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", order='" + order + '\'' +
                '}';
    }
}
