package com.dryl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

@WebServlet("/main")
public class MainServlet extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        File file = new File("/Users/dmytrodrul/IdeaProjects/myservlet/src/main/webapp/WEB-INF/hello.html");
        FileReader fr = new FileReader(file);
        Scanner scn = new Scanner(fr);
        int i =1;
        StringBuilder sb = new StringBuilder();
        while (scn.hasNextLine()){
            sb.append(scn.nextLine());
            i++;
        }
        fr.close();
        out.println(sb);
    }
}
