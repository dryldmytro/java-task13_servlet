package com.dryl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static com.dryl.Constant.*;

@WebServlet("/order")
public class OrderServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><head><body>");
        out.println("<form action='order' method='POST'> User Name: <input type='text' name='user_name'>" +
                "Order:<input type='text' name='user_order'>"
                +"<button type='submit'>Make order</button></form>");
        out.println("<h2><a href=\"main\">Back to menu</a></h2>");
        out.println("</head></body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("user_name");
        String userOrder = req.getParameter("user_order");
        User user = new User(userName,userOrder);
        UsersServlet u= new UsersServlet();
        u.doGet(req,resp);
    }
}
