package com.dryl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static com.dryl.Constant.*;
@WebServlet("/list")
public class UsersServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><head><body>");
        out.println("<table border='1'>");
        out.println("<tr><th>ID</td><th>User Name</td><th>User Order</td></tr>");
        for (User u:list) {
            int id = list.indexOf(u)+1;
            out.println("<tr><td>"+id+"</td>"+"<td>"+u.getName()+"</td>"+"<td>"+u.getOrder()+"</td></tr>");
        }
        out.println("</table>");
        out.println("<h2><a href=\"main\">Back to menu</a></h2>");
        out.println("<form action='list' method='POST'> User ID: <input type='text' name='user_id'>"
                +"<button type='submit'>Delete</button></form>");
        out.println("</head></body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doDelete(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("user_id");
        int intid= Integer.parseInt(id)-1;
        list.remove(intid);
        doGet(req, resp);
    }
}
