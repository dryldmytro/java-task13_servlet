package com.dryl;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebFilter("/order")
public class MyFilter implements Filter {
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain) throws IOException, ServletException {
        Optional<String> opt = Optional.ofNullable(req.getParameter("user_name"));
        if (opt.filter(user->user.equalsIgnoreCase("vova")).isPresent()){
            ((HttpServletResponse)resp).sendError(404,"Vova, not today");
            return;
        }
        filterChain.doFilter(req,resp);
    }
}
