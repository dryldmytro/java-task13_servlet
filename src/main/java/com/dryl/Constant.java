package com.dryl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Constant {
    public static Scanner scanner = new Scanner(System.in);
    public static Logger logger = LogManager.getLogger(Constant.class);
    public static ResourceBundle bundle = ResourceBundle.getBundle("project");
    public static List<User> list = new ArrayList<>();
}

